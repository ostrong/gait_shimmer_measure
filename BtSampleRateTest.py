#!/usr/bin/python
import sys, struct, array, serial
import pdb

from datetime import datetime


def wait_for_ack():
    ddata = ""
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)
    return

    if len(sys.argv) < 2:
        print "no device specified"
        print "You need to specify the serial port of the shimmer you wish to connect to"
        print "example:"
        print "  BtStreamAccelGyroMag.py Com5"
        print " or"
        print "  BtStreamAccelGyroMag.py /dev/rfcomm0"
    else:
        ser = serial.Serial(sys.argv[1], 115200, timeout=2)
        ser.flushInput()

# send the set sensors command
    ser.write(struct.pack('BBB', 0x07, 0xE0, 0x00))    # accel, gyro and mag
    wait_for_ack()

# send the set sampling rate command
#   ser.write(struct.pack('BB', 0x05, 0x64))           #  10.24Hz
#   ser.write(struct.pack('BB', 0x05, 0x14))           #  51.20Hz
    ser.write(struct.pack('BB', 0x05, 0x0A))           # 102.40Hz
#   ser.write(struct.pack('BB', 0x05, 0x04))           # 256.00Hz
    wait_for_ack()

    try:
        #get sampling rate command
        ser.write(struct.pack('B', 0x03))
        wait_for_ack()
        print "Ack received for get shimmer sampling rate command"

        ddata = ""
        shimmerversionresponse = struct.pack('B', 0x04)
        while ddata != shimmerversionresponse:
          ddata = ser.read(1)
        shimmerversion = struct.unpack('B', ser.read(1))
        print str(shimmerversion[0])

    except KeyboardInterrupt:
# send stop streaming command
        ser.write(struct.pack('B', 0x20));
        wait_for_ack()
# close the serial port
        ser.close()
        print
        print "All done"
        print z[0]
        print z[-1]
